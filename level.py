import pygame
from tile import Tile
from setting import tile_size

class Level:
    def __init__(self,level_data,surface):
        # Level setup
        self.display_surface = surface
        self.setup_level(level_data)
        self.world_shift = 0

    def setup_level(self,layout):
        self.tile = pygame.sprite.Group()
        for row_index,row in enumerate(layout):
            for col_index,cell in enumerate(row):
                if cell == "X":
                    x = col_index * tile_size
                    y = row_index * tile_size
                    tile = Tile((x,y),tile_size)
                    self.tile.add(tile)


    def run(self):
        self.tile.update(self.world_shift)
        self.tile.draw(self.display_surface)